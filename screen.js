// Smaller mode is experimental
// TODO: Allow the user to change the size (zoom)
const cellSize = urlParams.get("small") !== null ? [10.92, 21] : [13, 25];
let txtColor = "#00ff00";
let bgColor = "#0a0a0a";

class Screen {
  size = createVector();
  // cells contient les lignes de l'écran,
  // Chaque ligne contient ses cellules
  cells = [];
  events = {};
  focused = false;
  stdin;
  stdout;

  constructor(sizeX, sizeY) {
    this.size.x = sizeX || floor(windowWidth / cellSize[0]);
    this.size.y = sizeY || floor(windowHeight / cellSize[1]);

    for (let i = 0; i < this.size.y; i++) {
      this.cells.push([]);
      for (let j = 0; j < this.size.x; j++) {
        this.cells[i].push(new Cell(j * cellSize[0], i * cellSize[1], ""));
      }
    }

    this.stdin = new TerminalStream(this);
    // Temporairement, on ne fait pas de distinction entre les deux, et on écrit ou lit dans le même flux
    this.stdout = this.stdin;
  }

  focus() {
    this.dispAll();
    if (!canvasInput) throw "Une variable DOM canvasInput doit exister pour capturer les événements";
    canvasInput.focus();
    for (let evName of Object.keys(this.events)) {
      canvasInput.addEventListener(evName, this.events[evName]);
    }
    this.focused = true;
  }

  blur() {
    if (!canvasInput) throw "Une variable DOM canvasInput doit exister pour capturer les événements";
    for (let evName of Object.keys(this.events)) {
      canvasInput.removeEventListener(evName, this.events[evName]);
    }
    this.focused = false;
  }

  resize(x, y) {
    this.size.x = x;
    this.size.y = y;
    resizeCanvas(x * cellSize, y * cellSize);
  }

  inGrid(x, y) {
    return x >= 0 && x < this.size.x && y >= 0 && y < this.size.y;
  }

  dispRow(row) {
    if (row > this.size.y - 1) throw "Row out of bounds";

    for (let cell of this.cells[row]) {
      cell.render();
    }
  }

  dispAll() {
    for (let i = 0; i < this.size.y; i++) {
      this.dispRow(i);
    }
  }

  dispGrid() {
    stroke(0, 255, 0);

    for (let i = 0; i < this.size.y + 1; i++) {
      line(0, i * cellSize[1], width, i * cellSize[1]);
    }

    for (let j = 0; j < this.size.x + 1; j++) {
      line(j * cellSize[0], 0, j * cellSize[0], height);
    }

    noStroke();
  }

  getTextFomRow(row, ignoreSolid = false) {
    if (row > this.size.y - 1) throw "Row out of bounds";

    let fstr = "";
    for (let i = 0; i < this.cells[row].length; i++) {
      if (!(ignoreSolid && this.cells[row][i].solid)) fstr += this.cells[row][i].char;
    }
    return fstr;
  }

  getText(ignoreSolid = false) {
    let textString = "";

    for (let i = 0; i < this.size.y; i++) {
      textString += this.getTextFomRow(i, ignoreSolid);
      if (i !== this.size.y - 1) textString += "\n";
    }

    return textString;
  }

  clearRow(row, unsolidify = false) {
    if (row > this.size.y - 1) throw "Row out of bounds";

    for (let cell of this.cells[row]) {
      cell.setChar("");
      if (unsolidify) cell.unsolidify();
    }
  }

  clearAll(unsolidify = true) {
    for (let i = 0; i < this.size.y; i++) {
      this.clearRow(i, unsolidify);
    }
  }

  write(txt, x = 0, y = 0, wordWrap = true, solid = false, erase = false) {
    /* Si on essaye d'écrire au delà de l'écran (à droite), on
    balance une erreur */
    if (x >= this.size.x)
      throw ("Can't write that far right");

    // Si on essaye d'écrire du vide
    if (txt.length == 0) return [y, x];

    /* Si on a donné une liste de lignes au lieu d'une chaîne
    de caractères */
    if (txt instanceof Array) {
      txt = txt.join("\n");
    }

    // On gère x
    if (this.inGrid(0, y)) {
      txt = this.getTextFomRow(y).substr(0, x) + txt;
    } else {
      for (let i = 0; i < x; i++) txt = " " + txt;
    }

    // On gère les sauts de ligne
    let txtLines = txt.split(/\n/);

    // Les lignes à écrire
    let outLines = [];
    for (let l of txtLines) {
      outLines.push(this.splitInLines(l, wordWrap));
    }
    outLines = outLines.flat();

    // Écriture avec writeInRow
    let res = [y, x];
    for (let l of outLines) {
      res = this.writeInRow(y, l, 0, solid, erase);
      y = res[0] + 1;
    }

    /* On renvoie la position du curseur à la fin de l'écriture.
    En mode wordWrap, on a écrit une espace de trop, mais une espace est vide
    à l'affichage, on considère qu'on ne l'a pas écrite, et que le curseur
    est une case derrière */
    if (wordWrap) res[1]--;
    return res;
  }

  writeInRow(row, txt, xOffset = 0, solid = false, erase = false) {
    // Si le texte est trop large pour la ligne, on balance une erreur
    if (txt.length + xOffset > this.size.x)
      throw (`Texte (${txt.length} caractères) trop long (${term.cells[0].length - xOffset} max).`);

    /* Si on essaye d'écrire au delà de l'écran (en bas), on
    scrolle autant qu'il faut */
    if (row >= this.size.y) {
      let diff = row - this.size.y + 1;
      this.scroll(diff);
      row -= diff;
    }

    if (erase) this.clearRow(row);

    for (let i = 0; i < txt.length; i++) {
      if (solid) term.cells[row][i + xOffset].solidify();
      this.cells[row][i + xOffset].setChar(txt[i]);
    }

    // Renvoie la position de la dernière cellule écrite
    return [row, xOffset + txt.length];
  }

  scroll(n, pointer) {
    if (!n) n = 1;

    for (let i = 0; i < this.size.y; i++) {
      this.clearRow(i, true);
      if (i + n < this.size.y) this.writeInRow(i, this.getTextFomRow(i + n));
    }

    // On donne la possibilité de mettre un pointeur donné au bon endroit
    if (pointer) {
      pointer.at[0] = Math.max(0, pointer.at[0] - n);
      pointer.updatePos();
    }
  }

  splitInLines(text, wordWrap) {
    return Screen.splitInLines(text, this.size.x, wordWrap);
  }

  static splitInLines(text, sizeX, wordWrap = false) {
    if (wordWrap) {
      let lines = [];
      // Création d'un tableau sain de mots
      let words = text.split(/\s/);
      let wordsCut = [];
      for (let w of words) {
        wordsCut.push(Screen.splitInLines(w, sizeX));
      }
      words = wordsCut.flat().map(x => x == null ? "" : x);

      // Construction des lignes avec word wrap
      let currentLine = "";
      for (let w of words) {
        if (currentLine == "" && w.length == sizeX)
          lines.push(w);
        else if ((currentLine + w + " ").length > sizeX) {
          lines.push(currentLine);
          currentLine = w + " ";
        } else currentLine += w + " ";
      }
      if (currentLine != "") lines.push(currentLine.trimRight());
      return lines;
    } else {
      let reg = new RegExp(`.{1,${sizeX}}`, 'g');
      return text.match(reg);
    }
  }
}