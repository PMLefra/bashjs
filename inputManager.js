class InputManager {
  log = [];
  logHold = "";
  logIndex = 0;
  at = createVector(0, 0);
  text = "";
  screen;

  constructor(screen) {
    this.screen = screen;
  }

  setLog(newLog) {
    if (newLog.length >= InputManager.MAX_LOG_LENGTH)
      newLog = newLog.slice(newLog.length - InputManager.MAX_LOG_LENGTH);
    this.log = newLog;
  }

  addLogEntry(str) {
    this.log.push(str);
    if (this.log.length > InputManager.MAX_LOG_LENGTH) this.log.shift();
  }

  logPrev() {
    if (this.logIndex >= this.log.length) return;
    if (this.logIndex == 0) this.logHold = this.text;

    let newText;
    this.logIndex++;
    newText = this.log[this.log.length - this.logIndex];

    this.delAll();
    this.write(newText);
  }

  logNext() {
    if (this.logIndex <= 0) return;

    let newText;
    this.logIndex--;
    if (this.logIndex == 0) {
      newText = this.logHold;
      this.logHold = "";
    } else {
      newText = this.log[this.log.length - this.logIndex];
    }

    this.delAll();
    this.write(newText);
  }

  new() {
    if (this.text.trim().length > 0) {
      this.addLogEntry(this.text);
    }
    this.logIndex = 0;
    this.logHold = "";
    this.text = "";
  }

  addChar(char) {
    if (char.length > 1) throw "Expecting a single char";

    this.setCharAt(this.text.length, char);
  }

  addCharAt(index, char) {
    if (char.length > 1) throw "Expecting a single char";

    for (let i = this.screen.size.x - 1; i > this.at.x + index; i--) {
      this.setCharAt(i - this.at.x, this.screen.cells[this.at.y][i - 1].char);
    }
    this.setCharAt(index, char);
  }

  setCharAt(index, newChar) {
    if (newChar.length > 1) throw "Expecting a single char";
    if (this.at.x + index >= this.screen.size.x) throw "Can't set char: text too long";

    this.screen.cells[this.at.y][this.at.x + index].setChar(newChar);
    this.text = this.text.substr(0, index) + newChar + this.text.substr(index + 1);
  }

  delChar() {
    this.delCharAt(this.text.length - 1);
  }

  delCharAt(index) {
    if (this.at.x + index >= this.screen.size.x) throw "Can't delete char: out of bounds";
    if (this.screen.cells[this.at.y][this.at.x + index].solid) throw "Can't delete char: char is solid";

    for (let pos = this.at.x + index; pos < this.screen.size.x - 1; pos++) {
      let nextChar = this.screen.cells[this.at.y][pos + 1].char;
      this.screen.cells[this.at.y][pos].setChar(nextChar);
    }
    this.screen.cells[this.at.y][this.screen.size.x - 1].setChar("");

    this.text = this.text.substr(0, index) + this.text.substr(index + 1);
  }

  delAll() {
    while(this.text.length > 0) this.delChar();
  }

  write(text) {
    for (let char of text) {
      this.addChar(char);
    }
  }

  writeAt(index, text, overwrite = true) {
    for (let i = 0; i < text.length; i++) {
      if (overwrite) this.setCharAt(index + i, text[i]);
      else this.addCharAt(index + i, text[i]);
    }
  }

  get endX() {
    return this.at.x + this.text.length;
  }

  get length() {
    return this.text.length;
  }
}

InputManager.MAX_LOG_LENGTH = 500;