// Premier fichier defer qui récupère / gère les éléments DOM

// Un textarea invisible recouvre la page et gère tous les events,
// il est nécessaire pour que le clavier s'affiche sur mobile
let canvasInput = document.getElementById("canvasInput");

canvas.addEventListener("focus", function() {
  canvasInput.focus();
});