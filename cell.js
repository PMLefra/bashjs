class Cell {
  pos = createVector();
  char = "";
  solid = false;

  constructor(x, y, char) {
    this.pos.x = x;
    this.pos.y = y;
    this.char = char;
  }

  render() {
    this.clear();
    fill(txtColor);
    text(this.char, this.pos.x, this.pos.y + cellSize[1] - 5);
  }

  clear() {
    fill (bgColor);
    rect(this.pos.x, this.pos.y, cellSize[0], cellSize[1]);
  }

  setChar(c, render = true) {
    this.char = c;
    if (render) this.render();
  }

  solidify() {
    this.solid = true;
  }

  unsolidify() {
    this.solid = false;
  }
}