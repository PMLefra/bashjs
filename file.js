// A ne pas instancier !
class FileElement {
  name = "";
  path = "?";
  user = "";
  group = "";
  lastDate = new Date();
  parent;
  permissions = [0, 0, 0];

  static permissionBits = {
    1: "x",
    2: "w",
    4: "r",
    "x": 1,
    "w": 2,
    "r": 4
  };

  /* Une session doit être ouverte afin que User.current soit défini
  avant de faire appel à ce constructeur */
  constructor(name, user_ = User.current, group = "none") {
    if (name.includes("/")) throw "Can't create FileElement: forbidden character used";
    if (user_.trim().length == 0) user_ = User.current;
    this.name = name;
    this.user = user_;
    this.group = group;
  }

  getRoot() {
    let p = this;
    while (!p.isRoot && p.parent && p.parent != p) p = p.parent;
    return p;
  }

  updatePath(parentPath) {
    let nPath = `${parentPath}${parentPath == "/" ? "" : "/"}${this.name}`;
    if (this.path == "?") console.log(`Setting path of ${this.name} to ${nPath}`);
    else console.log(`Updating path of ${this.path} as ${nPath}`);
    this.path = nPath;

    // Si c'est un dossier, on parcourt récursivement ses enfants
    if (this.type == "dir") {
      for (let childName of Object.keys(this.children)) {
        if (childName != "." && childName != "..") {
          this.children[childName].updatePath(this.path);
        }
      }
    }
  }

  rename(newName) {
    let oldName = this.name;
    console.log(`Renaming ${this.path} as ${this.parent.path}${this.parent.isRoot ? "" : "/"}${newName}`);
    this.name = newName;
    this.parent.children[newName] = this;
    this.parent.children[oldName] = null;
    delete this.parent.children[oldName];
    this.updatePath(this.parent.path);
  }

  // dest est un FileElement
  move(dest, willBeRenamed = false) {
    if (this.path == dest.path) return false;
    let oldParent = this.parent;
    // Cette ligne est pour éviter d'écraser un fichier dans le cas où on fait un rename après le move
    if (willBeRenamed && dest.children.hasOwnProperty(this.name))
      // Franchement, aucune chance qu'un fichier dans dest s'apelle déjà comme ça :
      dest.children[this.name].rename(this.name + "-TMPMV$!$!");
    if (this.type == "dir") {
      this.parent = dest;
      this.children[".."] = this.parent;
    }
    dest.children[this.name] = this;
    oldParent.children[this.name] = null;
    delete oldParent.children[this.name];
    this.updatePath(dest.path);

    return true;
  }

  // where est un path (chaîne de caractères)
  copy(where, nameChange = "") {
    let cpName = nameChange || this.name;
    let returnValue = true;
    let rootDir = this.getRoot();
    let destDir = FileElement.findDir(rootDir, where);
    if (!where || !destDir) throw "Invalid destination";
    console.log(`Copying ${this.path} as ${where}${where[where.length - 1] == "/" ? "" : "/"}${cpName}`);

    if (this.type == "file") {
      destDir.createChild("file", cpName, this.buffer);
    } else if (this.type == "dir") {
      destDir.createChild("dir", cpName);
      let nDir = destDir.children[cpName];

      /* On créé récursivement les enfants du dossier à copier
      dans le dossier de destination */
      for (let childName of Object.keys(this.children)) {
        if (childName != "." && childName != "..") {
          let child = this.children[childName];
          if (FileElement.sameBase(nDir.path, child.path)) returnValue = false;
          else returnValue = child.copy(nDir.path) && returnValue;
        }
      }
    }

    return returnValue;
  }

  hasPermission(action, user) {  
    if (!user && User && User.current)
      user = User.current;
    else throw "Missing User context";

    if (!this.permissions) return;

    const bit = FileElement.permissionBits[action];
    if (!bit) return;
    
    if (user == this.user) return (this.permissions[0] & bit) != 0;
    // TODO: implement group permissions here
    else return (this.permissions[2] & bit) != 0;

    
    return (user == this.user && (this.permissions[0] & bit) != 0)
      || ((this.permissions[2] & bit) != 0);
  }

  static pathToArr(path) {
    let arr = path.split("/");
    if (arr[arr.length - 1] == "") arr.pop();
    return arr;
  }

  static getName(path) {
    return FileElement.pathToArr(path).pop();
  }

  static writeInFile(root, contentString, path, append = true) {
    let fileName = getName(path);
    let wdTree = FileElement.findDir(root, path, 1);

    if (wdTree == null)
      return errno.ENOENT;

    if (wdTree.hasChild(fileName)) {
      if (wdTree.children[fileName].type == "dir")
        return errno.EISDIR;
      else {
        if (append) wdTree.children[fileName].buffer += contentString;
        else wdTree.children[fileName].buffer = contentString;
        wdTree.children[fileName].lastDate = new Date();
      }
    } else wdTree.createChild("file", fileName, "", "", contentString);

    return 0;
  }

  static findDir(root, path, back = 0) {
    if (path[0] != "/") {
      // Si la variable globale cwd n'est pas définie...
      if (!cwd) throw "With no cwd, path must begin with /";
      path = `${cwd}${cwd[cwd.length - 1] == "/" ? "" : "/"}${path}`;
    }

    let pathArr = FileElement.pathToArr(path);
    if (back) pathArr.splice(pathArr.length - back, back);

    let wdTree;
    if (pathArr.length > 0 && pathArr[0] != "") {
      if (!cwd) throw "Requesting cwd, but cwd isn't set";
      wdTree = FileElement.findDir(root, cwd);
    } else {
      wdTree = root;
      pathArr.shift();
    }

    while (pathArr.length > 0) {
      if (!wdTree.children.hasOwnProperty(pathArr[0])) {
        // Invalid given path
        return null;
      }
      if (wdTree.children[pathArr[0]].type == "file") break;
      wdTree = wdTree.children[pathArr[0]];
      pathArr.shift();
    }

    return wdTree;
  }

  static findFile(root, path) {
    let pathArr = FileElement.pathToArr(path);
    let name = pathArr[pathArr.length - 1];
    let wdTree = FileElement.findDir(root, path);
    if (wdTree == null ||
      !wdTree.children.hasOwnProperty(name) ||
      wdTree.children[name].type == "dir") return null;
    return wdTree.children[name];
  }

  static find(root, path) {
    let pathArr = FileElement.pathToArr(path);
    let name = pathArr[pathArr.length - 1];
    let wdTree = FileElement.findDir(root, path);
    if (wdTree == null) return null;
    if (wdTree.children.hasOwnProperty(name) && wdTree.children[name].type == "file") return wdTree.children[name];
    return wdTree;
  }

  static sameBase(path1, path2) {
    let pathS, pathB;
    if (path1.length < path2.length) {
      pathS = path1;
      pathB = path2;
    } else {
      pathS = path2;
      pathB = path1;
    }
    for (let i = 0; i < pathS.length; i++) {
      if (pathS[i] != pathB[i]) return false;
    }
    return true;
  }

  // Renvoie une chaîne de caractères qui contient toutes les informations
  // sur le système de fichier de l'élément passé en argument
  static exportTree(treeEl) {
    let maxId = 0;
    // Enlève les parties récursives d'un élément en parcourant ses
    // enfants... récursivement, et en leur associant un id
    function removeRec(el, parentId) {
      // On fait d'abord une copie de l'élément, puis on remplace toutes
      // les références (pointeurs) par les id correspondants
      let nEl = {};
      Object.assign(nEl, el);
      nEl.id = maxId++;
      nEl.parent = parentId;

      if (nEl.type == "dir") {
        nEl.children = {};
        if (nEl.path != "/tmp") {
          for (let ch of Object.keys(el.children)) {
            if (ch != "." && ch != "..")
              nEl.children[ch] = removeRec(el.children[ch], nEl.id);
          }
        }
      }

      return nEl;
    }

    let rootDir = treeEl.getRoot();
    return JSON.stringify(removeRec(rootDir, 0));
  }

  // En entrée, une chaîne de caractères comme celle renvoyée par exportTree,
  // en sortie la racine (objet Directory) de l'arbre construit
  static importTree(treeStr) {
    // Tableau contenant tous les noeuds de l'arbre où l'indice est l'id
    let els = [];

    // Fonction récursive qui va parcourir l'arbre pour tout ranger dans els
    // et va retirer les id
    function explore(el) {
      els[el.id] = el;
      delete el.id;

      if (el.type == "dir") {
        for (let ch of Object.keys(el.children)) {
          if (ch != "." && ch != "..")
            explore(el.children[ch]);
        }
      }
    }

    // Fonction qui va reconstruire toutes les références, remettre
    // les prototypes et mettre la date sous forme d'objet
    function build(el) {
      el.parent = els[el.parent];
      el.lastDate = new Date(el.lastDate);

      if (el.type == "dir") {
        el.children["."] = el;
        el.children[".."] = el.parent;

        for (let ch of Object.keys(el.children)) {
          if (ch != "." && ch != "..")
            build(el.children[ch]);
        }
      }

      if (el.type == "file") Object.setPrototypeOf(el, File.prototype);
      else if (el.type == "dir") Object.setPrototypeOf(el, Directory.prototype);
    }

    let tree = JSON.parse(treeStr);
    explore(tree);
    build(tree);

    return tree;
  }
}

// Instancier l'une des deux classes suivantes

class Directory extends FileElement {
  type = "dir";
  permissions = [7, 7, 7];
  isRoot = false;
  children = {
    ".": this
  };

  constructor(name = "", user, group) {
    super(name, user, group);
    // Nom vide : on veut créer le dossier racine
    if (name == "") {
      this.isRoot = true;
      this.user = "root";
      this.group = "root";
      this.permissions = [7, 5, 5];
      this.path = "/";
      this.parent = this;
      this.children[".."] = this.parent;
    }
  }

  appendChild(el) {
    el.parent = this;
    el.updatePath(this.path)
    if (el.type == "dir") el.children[".."] = this;
    this.children[el.name] = el;
  }

  hasChild(name) {
    return this.children.hasOwnProperty(name);
  }

  createChild(type, name, user, group, buffer = "") {
    let el;
    if (type == "file") {
      el = new File(name, buffer, user, group);
    } else if (type == "dir") {
      el = new Directory(name, user, group);
    }

    this.appendChild(el);
  }

  removeChild(name) {
    if (!this.hasChild(name)) throw `No children named ${name}`;
    this.children[name] = null;
    delete this.children[name];
  }

  get childCount() {
    return Object.keys(this.children).length - 2;
  }
}

class File extends FileElement {
  type = "file";
  permissions = [6, 6, 6];
  buffer = "";

  constructor(name, buffer = "", user, group) {
    if (!name) throw "New file must have a name";
    super(name, user, group);
    this.buffer = buffer;
  }
}
