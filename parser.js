// This class is only used as a namespace, it is absolutely useless to instantiate it
class Parser {
  static NO_QUOTE = 0;
  static SINGLE_QUOTE = 1;
  static DOUBLE_QUOTE = 2;
  static NO_REDIRECT = 0;
  static REDIRECT_SIMPLE = 1;
  static REDIRECT_APPEND = 2;

  // Turns bash text into an array of parsed commands, in the same order as
  // they are in the text
  static parse(text) {
    // Sanitizes the input by keeping trimmed non-empty and non-comments lines,
    // even those that are split with an '\' at the end.
    text = text.replace(/\\\n/g, ' ');
    let lines = text.split('\n').map(el => el.trim()).filter(el => (el.length > 0 && el[0] != '#'));

    // Parses every line
    let parsedCommands = [];
    for (let line of lines)
      parsedCommands = parsedCommands.concat(this.parseLine(line));

    return parsedCommands;
  }

  // Turns the given line of text into an array of ParsedCommand objects
  // (that are finished, with either STATUS_PARSE_ERROR of STATUS_PARSED)
  static parseLine(line) {
    let words = this.getWords(line.trim());
    let commands = this.getCommands(words);
    this.parseCommands(commands);

    return commands;
  }

  // Separates a line of text into words (checks quotes too!)
  static getWords(line) {
    let words = [];
    let currentWord = "";
    let quoteMode = this.NO_QUOTE;
    let escape = false;

    for (let i = 0; i < line.length; i++) {
      let char = line[i];
      // Sets the "quoteMode" var to the correct value and adds the character
      // to the current word if necessary
      if (char === '\'') {
        // BUG: while processing quotes, we remove the meaning of single quoting or
        // double quoting, making it impossible to prevent expansions inside a single-
        // quoted string (for now). At least we cannot escape a single quote inside a
        // single quoted string, as expected, that's a start!
        if (quoteMode === this.NO_QUOTE) {
          if (escape) currentWord += char;
          else quoteMode = this.SINGLE_QUOTE;
        }
        else if (quoteMode === this.SINGLE_QUOTE) quoteMode = this.NO_QUOTE;
        else currentWord += char;
      } else if (char === '"' && !escape) {
        if (quoteMode === this.NO_QUOTE) quoteMode = this.DOUBLE_QUOTE;
        else if (quoteMode === this.DOUBLE_QUOTE) quoteMode = this.NO_QUOTE;
        else currentWord += char;
      }
      // Spacing: if not in a quoted string, ends the word,
      // pushing it to the "word" array if it isn't empty
      else if (/^\s$/.test(char)) {
        if (quoteMode === this.NO_QUOTE) {
          if (currentWord.length > 0)
            words.push(currentWord);
          currentWord = "";
        } else currentWord += char;
      }
      // Special command-delimiter characters, if not in a quoted string,
      // must be considered as whole words
      else if (/^[;|&><]$/.test(char)) {
        if (quoteMode === this.NO_QUOTE) {
          // Dirty, but works! Will clean this part later
          if (char === ';') {
            if (currentWord.length > 0)
              words.push(currentWord);
            words.push(char);
            currentWord = "";
          } else {
            if ((i + 1 < line.length) && line[i + 1] === char) {
              if (currentWord.length > 0)
                words.push(currentWord);
              words.push(char + char);
              currentWord = "";
              i++;
            } else {
              if (currentWord.length > 0)
                words.push(currentWord);
              words.push(char);
              currentWord = "";
            }
          }
        } else currentWord += char;
      }
      // In all the other cases except for the banned chars, the char is added to the current word
      else if (char !== '\\' || escape)
        currentWord += char;

      // The only case where escape doesn't change its value is if it was
      // decided false in the previous iteration and the current character isn't '\':
      // if it was decided true it will become false (can't double-escape) and if it is false
      // and the current char is '\', then it becomes true (that's the purpose of '\').
      if (char === '\\' || escape) escape = !escape;
    }
    // Pushes the last (possibly unfinished) word to the array
    if (currentWord.length > 0) words.push(currentWord);

    return words;
  }

  // Gets commands from a words array (one that is obtained with getWords)
  // Returns an unfinished parsed command (STATE_PARSING) array.
  static getCommands(words) {
    let commands = [];
    let currentCommand = new ParsedCommand;

    for (let word of words) {
      if (word == "&&") {
        commands.push(currentCommand);
        currentCommand = new ParsedCommand;
        currentCommand.requiredExitCode = 0;
      } else if (word == "||" || word == ";") {
        commands.push(currentCommand);
        currentCommand = new ParsedCommand;
      } else currentCommand.words.push(word);
    }
    // Pushes the last (possibly unfinished) command to the array
    commands.push(currentCommand);

    return commands;
  }

  // Performs aliases check, special operators parsing (e.g. ">"), expansions (e.g. "*")
  // and split into a command name ("key") and arguments
  static parseCommands(commands) {
    for (let command of commands) {
      if (command.words.length === 0
      || (command.words.length === 1 && command.words[0].length === 0))
        continue;

      let aliasWords = this.processAlias(command.words[0]);
      for (let word of aliasWords)
        command.arguments = command.arguments.concat(this.processExpansion(word));

      command.key = command.arguments.shift();

      let tmpRedirection = this.NO_REDIRECT;
      for (let word of command.words.slice(1)) {
        if (tmpRedirection !== this.NO_REDIRECT) {
          command.redirectMode = tmpRedirection;
          command.redirectTarget = word;
          tmpRedirection = this.NO_REDIRECT;
        } else if (word == ">") tmpRedirection = this.REDIRECT_SIMPLE;
        else if (word == ">>") tmpRedirection = this.REDIRECT_APPEND;
        else command.arguments = command.arguments.concat(this.processExpansion(word));
      }

      command.state = ParsedCommand.STATUS_PARSED;
    }
  }

  // Turns a given word (string) into an array of words that either only contains
  // the given word, or the words that replaces it if it is a valid alias
  // "aliases" is a global BashJS object variable
  static processAlias(word) {
    if (aliases.hasOwnProperty(word)) {
      return this.getWords(aliases[word]);
    } else return [word];
  }

  // Turns a given word (string) into an array of words that either only contains
  // the given word, or the words that replaces it if it could be expanded
  // TODO: process expansions when executing, not when parsing
  static processExpansion(word) {
    let expanded = [];

    // TODO: add all the missing expansions that exist in bash
    // TODO: search for some of the following patterns in a double-quoted-type string word and replace
    //  them with the expansion instead of treating all the words like they are of non-quoted-type
    if (word == "*") {
      for (let child of Object.keys(find(cwd).children))
        if (child[0] != ".") expanded.push(child);
    } else if (word == "~") {
      expanded.push(envVar["HOME"]);
    } else if (word == "!!") {
      if (input.log.length > 0)
        expanded.push(input.log[input.log.length - 2]);
    } else if (word[0] == '$' && word.length > 1) {
      let envVarName = word.slice(1);
      if (envVarName === '?') expanded.push(exitCode.toString());
      else if (envVar.hasOwnProperty(envVarName)) expanded.push(envVar[envVarName]);
      else expanded.push("");
    }

    if (expanded.length == 0) expanded.push(word);

    return expanded;
  }

  // Parses and returns the options from an array of words
  static getOptions(words) {
    let options = {
      list: [],
      order: ""
    };

    for (let word of words) {
      if (/^-[^\s-][^\s]*$/.test(word)) {
        for (let i = 1; i < word.length; i++) {
          options.list.push('-' + word[i]);
          options.order += "o";
        }
      } else if (/^--[^\s]+$/.test(word)) {
        options.list.push(word);
        options.order += "o";
      } else options.order += "a";
    }

    return options;
  }

  // Returns all the non-option words
  // whiteList: list of arguments that start with a '-' but should be
  //  considered as arguments instead of options
  static getNonOptions(words, whiteList = []) {
    let finalArray = [];
    for (let word of words)
      if (!/^--?[^\s]+$/.test(word) || whiteList.includes(word)) finalArray.push(word);

    return finalArray;
  }

  static test() {
    console.log(Parser.parse(`
      echo -n "lol ça va ?" "'hu  hu'"'ha'
      echo \\" hey "bien ou bien ?" "Salut \\" ça va ?" 'Oui \\' et toi ''
      false && echo 'Wow!' ; lsa *
      true&&echo "Waw";:
      # Ceci est un commentaire !
      
      pm-term --help
      
      cat js.js \
      > ks.js
    `));
  }
}

// Represents a command, parsing or parsed
class ParsedCommand {
  static STATUS_PARSING = 0;
  static STATUS_PARSED = 1;
  static STATUS_PARSE_ERROR = 2;

  state = ParsedCommand.STATUS_PARSING;
  key = "";
  words = [];
  arguments = [];
  redirectMode = Parser.NO_REDIRECT;
  redirectTarget = "";
  requiredExitCode = null;
}
