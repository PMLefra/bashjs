const BASHJS_VERSION = "v1.1.3";

function setup() {
  // Canvas
  createCanvas(windowWidth, windowHeight);

  // Paramètres de dessin
  noStroke();
  fill(txtColor);
  textSize(cellSize[1] - 3);
  textFont("JBMono");
  frameRate(0);

  // Initialisation
  background(bgColor);
  term.focus();
  document.fonts.ready.then(() => {
    // Splash text
    if (urlParams.get("ignoreSplash") === null) {
      pointer.at[0] = term.write([`Welcome to BashJS ${BASHJS_VERSION}!`, "Type 'help' to see the list of all available commands.", ""], pointer.at[1], pointer.at[0])[0] + 1;
    }

    promptAt(pointer.at[0]);
  });
}