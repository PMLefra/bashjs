class Stream {
  target;

  write(data) {}

  append(data) {}

  get size(){};

  read(size) {}

  flush() {}

  constructor(target) {
    if (target !== undefined) this.target = target;
  }
}

class TerminalStream extends Stream {
  write(data) {
    this.flush();

    pointer.at = [0, 0];
    pointer.updatePos();

    this.append(data);
  }

  append(data) {
    pointer.at = this.target.write(data, pointer.at[1], pointer.at[0]);
    pointer.at[1]++;
    if (pointer.at[1] >= this.target.size.x) {
      pointer.at[0]++;
      pointer.at[1] = 0;
    }
    pointer.updatePos();
  }

  get size() {
    return this.target.size.x * this.target.size.y;
  }

  read(size) {
    return this.target.getText().slice(0, size);
    // TODO: link screens to input managers, and add an input manager function to async read and return text
  }

  flush() {
    this.target.clearAll();
  }

  constructor(terminal) {
    super(terminal);
  }
}

class FileStream extends Stream {
  write(data) {
    return FileElement.writeInFile(rootDir, data, this.target, false);
  }

  append(data) {
    return FileElement.writeInFile(rootDir, data, this.target);
  }

  get size() {
    return findFile(this.target).buffer.length;
  }

  read(size) {
    return findFile(this.target).buffer.slice(0, size);
  }

  flush() {
    findFile(this.target).buffer = "";
  }

  constructor(path) {
    super(path);
  }
}

class VirtualStream extends Stream {
  write(data) {
    if (data instanceof Array) data = data.join("\n");
    this.target = data;
  }

  append(data) {
    if (data instanceof Array) data = data.join("\n");
    this.target += data;
  }

  get size() {
    return this.target.length;
  }

  read(size) {
    return this.target.slice(0, size);
  }

  flush() {
    this.target = "";
  }

  constructor() {
    super("");
  }
}