// @auto-fold regex
// noinspection JSUnfilteredForInLoop

// Variables propres au bash
let term = new Screen;
let input = new InputManager(term);
let pointer = new Pointer(term);
let prohibitedKeys = [9, 16, 17, 18, 20, 33, 34, 46, 91, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 221, 225];
let hostName = "bashjs";
let users = {
  "root": new User("root", "", 0, 0, "root", "/root")
}
let cwd = "/";
let exitCode = 0;
let envVar = {
  "HOME": "",
  "PATH": "/bin:/usr/bin",
  "PWD": cwd,
  "PS1": "\\u@\\h:\\w\\$ "
};
let aliases = {
  "c": "clear",
  "e": "exit",
  "lsa": "ls -lha"
};
let rootDir;

// Importation de fonctions
let wdToArr = FileElement.pathToArr;
let getName = FileElement.getName;
let sameBase = FileElement.sameBase;
let writeInFile = (contentString, path, append) => FileElement.writeInFile(rootDir, contentString, path, append);
let find = (path) => FileElement.find(rootDir, path);
let findFile = (path) => FileElement.findFile(rootDir, path);
let findDir = (path, back = 0) => FileElement.findDir(rootDir, path, back);

// Logging in as the default user before the fs is loaded
chUser("root");

/* Temporairement, pour les utilisateurs qui utilisent une ancienne version */
if (localStorage.hasOwnProperty("cmdlog")) {
  localStorage.removeItem("cmdlog");
  localStorage.removeItem("fs");
}

// Récupération ou construction de fichiers et dossiers: fs setup
if (localStorage.hasOwnProperty("fs")) {
  rootDir = FileElement.importTree(localStorage.getItem("fs"));
  loadPasswdUsers();
  if (users.hasOwnProperty("user")) chUser("user", true);
  else chUser("root", true);
} else {
  initFs();
  newUser("user");
  users["user"].info = "Main user";
  updatePasswdUser(findFile("/etc/passwd"), users["user"]);
  chUser("user", true);
}



function initFs() {
  rootDir = new Directory;

  rootDir.createChild("file", ".info", "root", "root", "Created with <3 by PMLefra <https://lefra.net> \nProject repo: https://gitlab.com/PMLefra/BashJS\n");
  rootDir.createChild("dir", "bin", "root");
  rootDir.createChild("dir", "boot", "root");
  rootDir.createChild("dir", "dev", "root");
  rootDir.createChild("dir", "etc", "root");
  rootDir.children["etc"].createChild("file", "passwd", "root");
  writePasswdUsers();
  rootDir.createChild("dir", "home", "root");
  for (let user in users) {
    createUserHome(users[user]);
  }
  rootDir.children["root"].createChild("file", "test", "root", "root", "A simple test file\nEasy, huh?\n");
  rootDir.createChild("dir", "mnt", "root");
  rootDir.createChild("dir", "opt", "root");
  rootDir.createChild("dir", "proc", "root");
  rootDir.createChild("dir", "tmp", "root");
  rootDir.createChild("dir", "usr", "root");
  rootDir.children["usr"].createChild("dir", "bin", "root");
  rootDir.children["usr"].children["bin"].createChild("file", "bash", "root", "none", "Were you expecting a quine?\n");
  rootDir.children["usr"].children["bin"].children["bash"].permissions = [7, 7, 7];
  rootDir.children["usr"].children["bin"].createChild("file", "js", "root", "none", "Use this file to execute JS code\n");
  rootDir.children["usr"].children["bin"].children["js"].permissions = [7, 7, 7];
  rootDir.createChild("dir", "var", "root");
  rootDir.children["var"].createChild("dir", "html", "root");
  rootDir.children["var"].children["html"].createChild("file", "test.html", "root", "",
    '<!DOCTYPE html><html lang="fr"><head><title>Test page</title><meta charset="utf-8"></head><body><h1>Ceci est un test !</h1></body></html>');
}

function addPasswdUser(passwdFile, user) {
  passwdFile.buffer += `${user.username}:${user.password}:${user.uid}:${user.gid}:${user.info}:${user.homeDirectory}:${user.shell}\n`;
}

function delPasswdUser(passwdFile, userName) {
  let passwdUsers = passwdFile.buffer.split('\n');
  for (let i = passwdUsers.length - 1; i >= 0; i--) {
    if (passwdUsers[i].length == 0) continue;
    if (passwdUsers[i].split(':')[0] == userName) passwdUsers.splice(i, 1);
  }
  passwdFile.buffer = passwdUsers.join('\n');
}

function updatePasswdUser(passwdFile, user) {
  delPasswdUser(passwdFile, user.username);
  addPasswdUser(passwdFile, user);
}

function loadPasswdUsers() {
  if (!rootDir) throw "Can't load users: file system not set";
  let passwdFile = findFile("/etc/passwd");
  if (passwdFile == null) throw "Can't load users: /etc/passwd does not exist";

  users = {};
  let usersStr = passwdFile.buffer.split('\n');
  for (let userStr of usersStr) {
    if (userStr.length == 0) continue;
    let userArr = userStr.split(':');
    User.lastUID = Math.max(User.lastUID, Number(userArr[2]) + 1);
    users[userArr[0]] = new User(userArr[0], userArr[1], userArr[2], userArr[3], userArr[4], userArr[5], userArr[6]);
  }
}

function writePasswdUsers() {
  if (!rootDir) throw "Can't save users: file system not set";
  let passwdFile = findFile("/etc/passwd");
  if (passwdFile == null) throw "Can't save users: /etc/passwd does not exist";

  passwdFile.buffer = "";
  for (let userName in users) {
    let user = users[userName];
    addPasswdUser(passwdFile, user);
  }
}

function newUser(nUser) {
  // Forbidden characters
  if (nUser.includes('/') || nUser.includes(' ')) throw "Can't create user: forbidden character used";
  if (users.hasOwnProperty(nUser)) throw "Can't create user: user already exists";
  if (!rootDir) throw "Can't add user: file system not set";

  let passwdFile = findFile("/etc/passwd");
  if (passwdFile == null) {
    rootDir.children["etc"].createChild("file", "passwd", "root");
    passwdFile = findFile("/etc/passwd");
  }

  users[nUser] = new User(nUser, "", -1, -1, nUser, `/home/${nUser}`);

  createUserHome(users[nUser]);

  addPasswdUser(passwdFile, users[nUser]);
}

function createUserHome(user) {
  let parent = findDir(user.homeDirectory, 1);
  let dirName = getName(user.homeDirectory);
  if (parent == null) throw "No such directory";

  if (!parent.hasChild(dirName)) {
    parent.createChild("dir", dirName, user.username);
    let homeDir = parent.children[dirName];
    homeDir.createChild("file", ".bash_history", user.username);
  }
}

function delUser(user) {
  if (user == "root") throw "Can't delete root user";
  if (!users.hasOwnProperty(user)) throw `${user}: no such user`;
  if (!rootDir) throw "Can't delete user: file system not set";

  let passwdFile = findFile("/etc/passwd");
  if (passwdFile == null) throw "Can't delete user: /etc/passwd does not exist";

  // In case the user has one or multiple active sessions
  for (let i = User.sessions.length - 1; i >= 0; i--) {
    if (User.sessions[i] == user) User.sessions.splice(i, 1);
  }
  if (User.sessions.length == 0) chUser("root");

  delete users[user];
  delPasswdUser(passwdFile, user);
}

function chUser(nUser, sameSession = false) {
  if (!users.hasOwnProperty(nUser)) throw `${nUser}: no such user`;

  if (sameSession && User.sessions.length > 0) User.sessions.pop();
  User.sessions.push(nUser);
  envVar["HOME"] = users[nUser].homeDirectory;
  cwd = envVar["HOME"];
  envVar["PWD"] = cwd;

  if (rootDir) {
    // Chargement de l'historique des commandes
    let cmdLogFile = findFile(`${envVar["HOME"]}/.bash_history`);
    if (cmdLogFile) {
      let cmdLog = cmdLogFile.buffer.split('\n');
      cmdLog.pop();
      input.setLog(cmdLog);
      // Pour s'assurer que l'historique ne dépasse pas InputManager.MAX_LOG_LENGTH
      cmdLogFile.buffer = input.log.join('\n') + '\n';
    }

    // Chargement du profil (oui, je sais, c'est .bash_profile normalement, mais on reste
    // sur .bashrc pour l'instant)
    let profileFile = findFile(`${envVar["HOME"]}/.bashrc`);
    if (profileFile) {
      console.log(profileFile.buffer);
      exec(profileFile.buffer, false);
    }
  }
}

function promptAt(row, append = false) {
  let xOffset = append ? term.getTextFomRow(row).length : 0;

  for (let i = 0; i < xOffset; i++)
    term.cells[row][i].solidify();

  let promptSymbol = User.current == "root" ? '#' : '$';
  let displayedCwd = envVar["PWD"];
  let replaceHome = new RegExp(`^${envVar["HOME"]}`);
  displayedCwd = displayedCwd.replace(replaceHome, "~");
  let promptText = "";
  //TODO: add all the remaining substitutes for $PS1
  if (envVar["PS1"])
    promptText = envVar["PS1"]
      .replace(/\\u/g, User.current)
      .replace(/\\H/g, hostName)
      .replace(/\\h/g, hostName.split(".")[0])
      .replace(/\\w/g, displayedCwd)
      .replace(/\\s/g, "bash")
      .replace(/\\\$/g, promptSymbol);
  pointer.at = term.writeInRow(row, promptText, xOffset, true, !append);
  pointer.updatePos();
  input.at.x = pointer.at[1];
  input.at.y = pointer.at[0];
}

window.onunload = function() {
  writePasswdUsers();
  localStorage.setItem("fs", FileElement.exportTree(rootDir));
}

term.events.paste = function(e) {
  e.preventDefault();
  let textData = e.clipboardData.getData("Text");
  input.writeAt(pointer.at[1] - input.at.x, textData);
  pointer.at[1] += textData.length;
  pointer.updatePos();
}

if (isMobile) alert("Bon, les events de clavier sur mobile sont si mal fichus que j'ai pas terminé la compatibilité mobile de ce terminal. Désolé !");
else term.events.keydown = function(e) {
  let keyCode = e.keyCode;

  // Backspace
  if (keyCode == 8) {
    e.preventDefault();

    if (pointer.at[1] > 0 && !term.cells[pointer.at[0]][pointer.at[1] - 1].solid) {
      pointer.at[1]--;
      pointer.updatePos();
      input.delCharAt(pointer.at[1] - input.at.x);
    }
  }
  // Delete (suppr)
  else if (keyCode == 46) {
    e.preventDefault();
    if (input.length > 0) {
      input.delCharAt(pointer.at[1] - input.at.x);
    }
  }
  // Return (entrée)
  else if (keyCode == 13) {
    /* On récupère le texte de la commande, puis on réinitialise l'input (avec ajout à l'historique),
    puis on déplace le curseur, puis on parse + exécute la commande */
    let cmd = input.text.trimLeft();

    if (cmd.length > 0) {
      let homeDir = findDir(envVar["HOME"]);
      if (!homeDir.hasChild(".bash_history"))
        homeDir.createChild("file", ".bash_history", User.current);
      let logCmdFile = homeDir.children[".bash_history"];
      new FileStream(homeDir.children[".bash_history"].path).append(cmd + '\n');
    }
    input.new();
    pointer.at[0]++;
    pointer.at[1] = 0;
    if (pointer.at[0] >= term.size.y) term.scroll(1, pointer);
    pointer.updatePos();

    exec(cmd, true);
  }
  // Insert
  else if (keyCode == 45) {
    e.preventDefault();
    pointer.insert = !pointer.insert;
    pointer.text = pointer.insert ? "_" : "|";
    fill(251, 255, 43);
    setTimeout(() => fill(txtColor), 500);
  }
  // Home (début)
  else if (keyCode == 36) {
    while ((pointer.at[1] - 1 >= 0) && !term.cells[pointer.at[0]][pointer.at[1] - 1].solid) {
      pointer.at[1]--;
    }
    pointer.updatePos();
  }
  // End (fin)
  else if (keyCode == 35) {
    while ((pointer.at[1] + 1 < term.cells[0].length) && term.cells[pointer.at[0]][pointer.at[1]].char != "") {
      pointer.at[1]++;
    }
    pointer.updatePos();
  }
  // Tab
  else if (keyCode == 9) {
    e.preventDefault();
    let words = input.text.substr(0, pointer.at[1] - input.at.x).split(" ");
    let lastWord = words[words.length - 1];
    let currentDir = find("");
    let possibleWords = [];

    // Fonction qui sera déplacée plus tard, qui permet de compléter un chemin
    function autoCompletePath(incompletePath) {
      let relative = (incompletePath[0] != '/');
      let replacePathBeginning = "";

      if (relative) {
        // On garde en mémoire ce qu'on a ajouté au début pour le retirer à la fin
        replacePathBeginning = `${currentDir.path}${currentDir.path == '/' ? '' : '/'}`;
        incompletePath = replacePathBeginning + incompletePath;
      }

      let possible = [];
      let searchWd;
      let searchName;

      if (incompletePath[incompletePath.length - 1] == "/") {
        searchWd = findDir(incompletePath);
        searchName = "";
      }
      else {
        searchWd = findDir(incompletePath, 1);
        searchName = getName(incompletePath);
      }

      function endWord(fileName, hidden = false) {
        let word;
        /* 👇 Doing this for hidden files instead of searchWd.children[name].path
        because of the particular cases of . and .., as their stored paths are not
        /something/. and /something/.. */
        if (hidden) word = `${searchWd.path == "/" ? "" : searchWd.path}/${fileName}`
        else word = searchWd.children[fileName].path;

        word += (searchWd.children[fileName].type == "dir" ? '/' : '');

        // On enlève le début du chemin dans le cas de l'auto-complétion relative
        if (relative) {
          let replacePathBeginningRegex = new RegExp(`^${replacePathBeginning}`);
          word = word.replace(replacePathBeginningRegex, '');
        }

        possible.push(word);
      }

      // Si le dossier existe, on parcourt tous ses enfants
      if (searchWd != null) {
        for (let fileName in searchWd.children) {
          if (fileName[0] == '.') {
            // Checking if the user wants hidden file auto-completion
            if (searchName.length > 0 && searchName[0] == '.') {
              endWord(fileName, true);
            }
          } else endWord(fileName);
        }
      }

      return possible;
    }

    // On considère qu'on cherche à compléter un chemin, relatif ou absolu, si on a écrit un /
    if (lastWord.includes("/")) {
      possibleWords = autoCompletePath(lastWord);
    } else {
      /* Si on ne cherche pas à compléter un chemin, on propose
      - les fichiers et dossiers du dossier actuel
      - les commandes
      - les aliases */
      for (let childName in currentDir.children) {
        if (childName[0] != '.' || lastWord[0] == '.')
          possibleWords.push(childName + (currentDir.children[childName].type == "dir" ? '/' : ''));
      }
      /* Commandes et aliases uniquement si c'est le premier mot, ou après sudo.
      Plus tard, les commandes communiqueront leurs catégories d'auto-complétion pour
      leurs arguments. */
      if (words.length == 1 || (words.length == 2 && words[0] == "sudo")) {
        possibleWords = possibleWords.concat(Object.keys(commands));
        possibleWords = possibleWords.concat(Object.keys(aliases));
      }
    }

    // On ne garde que les possibilités qui correspondent à ce qu'on a écrit
    possibleWords = possibleWords.filter(possibleWord => {
      return possibleWord.substr(0, lastWord.length) == lastWord;
    });

    // Si il n'y a qu'une possibilité, on l'écrit.
    if (possibleWords.length == 1 && possibleWords[0].length > lastWord.length) {
      let remaining = possibleWords[0].substr(lastWord.length);
      input.writeAt(pointer.at[1] - input.at.x, remaining, false);

      pointer.at[1] += remaining.length;
      pointer.updatePos();
    }
  }
  // Ctrl+G
  else if (e.ctrlKey && keyCode == 71) {
    e.preventDefault();
    term.dispGrid();
  }
  // Ctrl+L
  else if (e.ctrlKey && keyCode == 76) {
    e.preventDefault();
    exec("clear", true);
  }
  // Ctrl+D
  else if (e.ctrlKey && keyCode == 68) {
    e.preventDefault();
    term.stdin.append("exit\n");
    exec("exit", true);
  }
  // Ctrl+U
  else if (e.ctrlKey && keyCode == 85) {
    e.preventDefault();

    while (pointer.at[1] > 0 && !term.cells[pointer.at[0]][pointer.at[1] - 1].solid) {
      pointer.at[1]--;
      pointer.updatePos();
      input.delCharAt(pointer.at[1] - input.at.x);
    }
  }
  // Arrows
  else if (keyCode == 38) {
    input.logPrev();
    pointer.at[1] = input.endX;
    pointer.updatePos();
  } else if (keyCode == 40) {
    input.logNext();
    pointer.at[1] = input.endX;
    pointer.updatePos();
  } else if (keyCode == 37) {
    if (pointer.at[1] > 0 && !term.cells[pointer.at[0]][pointer.at[1] - 1].solid) {
      pointer.at[1]--;
      pointer.updatePos();
    }
  } else if (keyCode == 39) {
    if (pointer.at[1] < term.size.x - 1 && term.cells[pointer.at[0]][pointer.at[1]].char != "") {
      pointer.at[1]++;
      pointer.updatePos();
    }
  }
  //Autre non interdit
  else if (prohibitedKeys.lastIndexOf(keyCode) == -1 && !e.ctrlKey && !(e.key == "Dead")) {
    if (keyCode != 86 && keyCode != 73) e.preventDefault();

    if (pointer.insert) {
      input.setCharAt(pointer.at[1] - input.at.x, e.key[0]);
    } else {
      input.addCharAt(pointer.at[1] - input.at.x, e.key[0]);
    }
    if (pointer.at[1] < term.size.x - 1) {
      pointer.at[1]++;
      pointer.updatePos();
    }
  }
}
