let maxCommandId = 0;

// TODO: move commands inside the BashJS fs (.js files) and make them accessible from the PATH :^)
let commands = {
  ":": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      return 0;
    }
  },

  "source": {
    id: maxCommandId++,
    exec: function(stdout, args) {
      stdout.append("Sourcing not yet implemented\n");
      return 0;
    }
  },

  ".": {
    id: maxCommandId++,
    exec: function(stdout, args) {
      return commands["source"].exec(stdout, args);
    }
  },

  "help": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let commandList = Object.keys(commands);
      commandList.sort();
      stdout.append(["Command list:", commandList.join(" "), ""]);

      return 0;
    }
  },

  "clear": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      term.clearAll();
      pointer.at = [0, 0];
      pointer.updatePos();

      return 0;
    }
  },

  "echo": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let options = Parser.getOptions(args).list;
      args = Parser.getNonOptions(args);
      if (options.includes("-n")) stdout.append(args.join(" "));
      else stdout.append([args.join(" "), ""]);
      return 0;
    }
  },

  "ping": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      stdout.append(["pong! (sorry, no networking yet)", ""]);
      return 0;
    }
  },

  "pwd": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      stdout.append([cwd, ""]);
      return 0;
    }
  },

  "ls": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let options = Parser.getOptions(args).list;
      args = Parser.getNonOptions(args);

      let wd = args[0] ?? cwd;
      let dispSpecial = options.includes("-a");

      let wdTree = find(wd);
      if (wdTree == null) {
        stdout.append([`ls: ${wd}: ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }

      // Option de format -l
      if (options.includes("-l")) {
        function makeStr(file, dispName, userLength = 0) {
          let str = file.type == "file" ? "-" : "d";
          for (let i = 0; i < 3; i++) {
            str += file.permissions[i] & 4 ? "r" : "-";
            str += file.permissions[i] & 2 ? "w" : "-";
            str += file.permissions[i] & 1 ? "x" : "-";
          }
          // str + " " + nombre de liens physiques;
          str += " " + file.user;
          if (userLength != 0) {
            for (let i = file.user.length; i < userLength; i++) str += " ";
          }
          // str += " " + file.group;
          // str += " " + taille du fichier;

          // Ce qui suit est pour le format de la date. Pfiou !
          str += " " + file.lastDate.toLocaleString("en-us", {
            month: "short" // Pour avoir genre Jan, Feb...
          });
          if (file.lastDate.getDate() < 10) str += " ";
          str += " " + file.lastDate.getDate();
          if (file.lastDate.getYear() == new Date().getYear()) {
            str += " " + file.lastDate.toLocaleString("en-us", {
              hour: "2-digit",
              minute: "2-digit",
              hour12: false
            });
          } else {
            str += "  " + file.lastDate.getFullYear();
          }

          str += " " + dispName;
          return str;
        }

        if (wdTree.type == "file") {
          stdout.append([makeStr(wdTree, wdTree.name), ""]);
          return 0;
        }

        // Pour le format de l'utilisateur, afin que tout soit aligné.
        let longestUserLength = 0;
        for (let f of Object.keys(wdTree.children)) {
          if ((f[0] != "." || dispSpecial) && wdTree.children[f].user.length > longestUserLength)
            longestUserLength = wdTree.children[f].user.length;
        }

        let output = [];
        for (let f of Object.keys(wdTree.children)) {
          if (f[0] != "." || dispSpecial) output.push(makeStr(wdTree.children[f], f, longestUserLength));
        }

        output.sort(function (a, b) {
          let aCols = a.split(/\s+/),
            bCols = b.split(/\s+/);
          return aCols[aCols.length - 1].localeCompare(bCols[bCols.length - 1]);
        });
        output.push("");

        if (output.length > 1) stdout.append(output);
        return 0;
      }

      // Sans option de format
      else {
        if (wdTree.type == "file") {
          stdout.append([wdTree.name, ""]);
          return 0;
        }

        let children = Object.keys(wdTree.children);
        children.sort((a, b) => a.localeCompare(b));

        let str = "";
        for (let child of children) {
          if (child[0] != "." || dispSpecial) str += child + " ";
        }

        if (str.trim() != "") {
          stdout.append([str, ""]);
        }

        return 0;
      }
    }
  },

  "sl": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      window.open("https://www.youtube.com/watch?v=GnrwM7vFn_U");
      return 0;
    }
  },

  "cd": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      if (args.length > 1) {
        stdout.append([`cd: too many arguments`, ""]);
        return errno.E2BIG;
      }
      let wd = args[0] || envVar["HOME"];
      let wdTree = find(wd);
      if (wdTree == null) {
        stdout.append([`sh: cd: ${wd}: ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }
      if (wdTree.type == "file") {
        stdout.append([`sh: cd: ${wd}: ${errnoMsg.ENOTDIR}`, ""]);
        return errno.ENOTDIR;
      }
      cwd = wdTree.path;
      envVar["PWD"] = cwd;

      return 0;
    }
  },

  "cat": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      if (args.length == 0) {
        stdout.append([`cat: missing file operand`, ""]);
        return errno.E2SMALL;
      }

      let exit = 0;
      let concat = "";
      for (let arg of args) {
        let file = find(arg);
        if (file == null) {
          stdout.append([`cat: ${arg}: ${errnoMsg.ENOENT}`, ""]);
          exit = errno.ENOENT;
          continue;
        }
        if (file.type == "dir") {
          stdout.append([`cat: ${arg}: ${errnoMsg.EISDIR}`, ""]);
          exit = errno.EISDIR;
          continue;
        }

        concat += file.buffer;
      }

      stdout.append(concat.split("\n"));
      return exit;
    }
  },

  "mkdir": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append([`mkdir: missing operand`, ""]);
        return errno.E2SMALL;
      }

      for (let arg of args) {
        let name = getName(arg);
        let dir = findDir(arg, 1);
        if (dir == null) {
          stdout.append([`mkdir: can't create directory '${arg}': ${errnoMsg.ENOENT}`, ""]);
          return errno.ENOENT;
        }
        if (dir.hasChild(name)) {
          stdout.append([`mkdir: can't create directory '${arg}': ${errnoMsg.EEXIST}`, ""]);
          return errno.EEXIST;
        }
        else dir.createChild("dir", name);
      }

      return 0;
    }
  },

  "touch": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      if (args.length == 0) {
        stdout.append([`touch: missing file operand`, ""]);
        return errno.E2SMALL;
      }

      for (let arg of args) {
        let name = getName(arg);
        let dir = findDir(arg, 1);
        if (dir == null) {
          stdout.append([`touch: cannot touch '${arg}': ${errnoMsg.ENOENT}`, ""]);
          return errno.ENOENT;
        }
        else if (dir.hasChild(name)) dir.children[name].lastDate = new Date();
        else dir.createChild("file", name);
      }

      return 0;
    }
  },

  "sudo": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let parsedOptions = Parser.getOptions(args);
      args = Parser.getNonOptions(args);

      let cmd = "",
        optionsBefore = [];
      let i = 0,
        iOpt = 0,
        iArg = 0,
        length = parsedOptions.list.length + args.length;
      while (i < length) {
        if (parsedOptions.order[i] == "o") {
          if (cmd != "") cmd += "--" + parsedOptions.list[iOpt] + " ";
          else optionsBefore.push(parsedOptions.list[iOpt]);
          iOpt++;
        } else {
          cmd += args[iArg] + " ";
          iArg++;
        }
        i++;
      }

      if (optionsBefore.includes("i") || optionsBefore.includes("s")) {
        chUser("root");
      } else if (cmd.length > 0) {
          User.sessions.push("root");
          exec(cmd, false);
          User.sessions.pop();
      }

      return 0;
    }
  },

  "rm": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let options = Parser.getOptions(args).list;
      args = Parser.getNonOptions(args);

      let recursive = (options.includes("-r") || options.includes("-R"));
      if (args.length == 0) {
        stdout.append(["rm: missing operand", ""]);
        return errno.E2SMALL;
      }

      let exit = 0;

      for (let element of args) {
        let file = getName(element);
        let wdTree = findDir(element, 1);
        if (element == "/") file = ".";
        if (wdTree == null || !wdTree.hasChild(file)) {
          stdout.append(`rm: cannot remove '${element}': ${errnoMsg.ENOENT}\n`);
          exit = errno.ENOENT;
        }
        else if (!recursive && wdTree.children[file].type == "dir") {
          stdout.append(`rm: cannot remove '${element}': ${errnoMsg.EISDIR}\n`);
          exit = errno.EISDIR;
        }
        else if (element == "/") {
          stdout.append(`rm: refusing to remove '/'\n`);
          exit = errno.EPERM;
        }
        else if (element == "." || element == "..") {
          stdout.append(`rm: refusing to remove '.' or '..' directory: skipping '${element}'\n`);
          exit = errno.EPERM;
        }
        else {
          console.log("Deleting " + wdTree.children[file].path);
          wdTree.removeChild(file);
        }
      }

      return exit;
    }
  },

  "rmdir": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append([`rmdir: missing operand`, ""]);
        return errno.E2SMALL;
      }

      let exit = 0;

      for (let arg of args) {
        let file = getName(arg);
        let wdTree = findDir(arg, 1);
        // Pas opti pour les deux prochaines lignes...
        let toRemove = find(arg);
        if (toRemove && toRemove.path == "/") {
          stdout.append([`rmdir: failed to remove '${arg}': ${errnoMsg.EBUSY}`, ""]);
          exit = errno.EBUSY;
        }
        else if (wdTree == null || !wdTree.hasChild(file)) {
          stdout.append([`rmdir: failed to remove '${arg}': ${errnoMsg.ENOENT}`, ""]);
          exit = errno.ENOENT;
        }
        else if (wdTree.children[file].type == "file") {
          stdout.append([`rmdir: failed to remove '${arg}': ${errno.ENOTDIR}`, ""]);
          exit = errno.ENOTDIR;
        }
        else if (wdTree.children[file].childCount > 0) {
          stdout.append([`rmdir: failed to remove '${arg}': ${errnoMsg.ENOTEMPTY}`, ""]);
          exit = errno.ENOTEMPTY;
        }
        else {
          console.log("Deleting " + wdTree.children[file].path);
          wdTree.removeChild(file);
        }
      }

      return exit;
    }
  },

  "cp": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let options = Parser.getOptions(args).list;
      args = Parser.getNonOptions(args);

      // Init
      if (args.length < 2) {
        stdout.append(["cp: missing file operand", ""]);
        return errno.E2SMALL;
      }
      let recursive = (options.includes("-r") || options.includes("-R"));
      // Source
      let fileSource = getName(args[0]);
      if (fileSource == "") fileSource = "."; // Cas de "/"
      let wdTreeSource = findDir(args[0], 1);
      if (wdTreeSource == null || !wdTreeSource.hasChild(fileSource)) {
        stdout.append([`cp: can't stat '${args[0]}': ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }
      if (!recursive && wdTreeSource.children[fileSource].type == "dir") {
        stdout.append([`cp: omitting directory '${fileSource}'`, ""]);
        return errno.EINVAL;
      }
      let toCopy = wdTreeSource.children[fileSource];
      // Destination
      let fileDest = getName(args[1]);
      let wdTreeDest = findDir(args[1], 1);
      if (wdTreeDest == null) {
        stdout.append([`cp: can't create '${args[1]}': ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }
      if (wdTreeDest.hasChild(fileDest)) {
        if (wdTreeDest.children[fileDest].type == "dir") {
          if (wdTreeSource.path == "/" && fileSource == ".") {
            stdout.append([`cp: please rename / while copying it`, ""]);
            return errno.EINVAL;
          }
          wdTreeDest = wdTreeDest.children[fileDest];
          fileDest = "";
        } else {
          wdTreeDest.children[fileDest] = null;
          delete wdTreeDest.children[fileDest];
        }
      }
      // Copie
      if (!toCopy.copy(wdTreeDest.path, fileDest)) {
        stdout.append([`cp: cannot copy a directory into itself`, ""]);
        return errno.EINVAL;
      }

      return 0;
    }
  },

  "su": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      let newUser = args.length > 0 ? args[0] : "root";
      if (!users.hasOwnProperty(newUser)) {
        stdout.append([`su: user '${newUser}' does not exist or the user entry does not contain all the required fields`, ""]);
        return errno.EINVAL;
      }

      chUser(newUser);

      return 0;
    }
  },

  "useradd": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append(["useradd: missing operand", ""]);
        return errno.E2SMALL;
      }
      if (User.current != "root") {
        stdout.append(["Permission denied.", "Please run this command with root privileges.", ""]);
        return errno.EPERM;
      }
      if (args[0].includes('/') || args[0].includes(' ')) {
        stdout.append([`useradd: invalid user name '${args[0]}'`, ""]);
        return errno.EINVAL;
      }
      if (users.hasOwnProperty(args[0])) {
        stdout.append([`useradd: user '${args[0]}' already exists`, ""]);
        return errno.EINVAL;
      }

      newUser(args[0]);
      stdout.append([`User '${args[0]}' created`, ""]);

      return 0;
    }
  },

  "userdel": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append(["userdel: missing operand", ""]);
        return errno.E2SMALL;
      }
      if (User.current != "root") {
        stdout.append(["Permission denied.", "Please run this command with root privileges.", ""]);
        return errno.EPERM;
      }
      if (args[0] == "root") {
        stdout.append(["userdel: can't delete root user", ""]);
        return errno.EINVAL;
      }
      if (!users.hasOwnProperty(args[0])) {
        stdout.append([`userdel: ${user}: no such user`, ""]);
        return errno.EINVAL;
      }

      delUser(args[0]);
      stdout.append([`User '${args[0]}' deleted`, ""]);

      return 0;
    }
  },

  "whoami": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      stdout.append([User.current, ""]);
      return 0;
    }
  },

  "hostname": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      stdout.append([hostName, ""]);
      return 0;
    }
  },

  "env": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let out = [];
      for (let key in envVar) {
        out.push(`${key}=${envVar[key]}`);
      }
      out.push("");
      stdout.append(out);

      return 0;
    }
  },

  "date": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let format = {
        hour12: false,
        weekday: "short",
        month: "short",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZoneName: "short",
        year: "numeric"
      };

      let dateString = new Date().toLocaleString("en", format);
      dateString = dateString.replace(/,/g, "");

      let dateParts = dateString.split(" ");
      dateParts[2] = dateParts[2].replace(/^0/, " ");
      dateParts.push(dateParts.splice(3, 1));

      dateString = dateParts.join(" ");

      stdout.append([dateString, ""]);
      return 0;
    }
  },

  "exit": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      User.sessions.pop();
      if (User.sessions.length == 0) window.location = "..";
      else chUser(User.sessions.pop());

      return 0;
    }
  },

  "logout": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      // Hé hé hé ha ha
      exec("exit", false);
      return 0;
    }
  },

  "mv": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      // Init
      if (args.length < 2) {
        stdout.append(["mv: missing operand", ""]);
        return errno.E2SMALL;
      }
      // Source
      let fileSource = getName(args[0]);
      if (fileSource == "") { // Cas de "/"
        stdout.append([`mv: cannot move '${args[0]}' to '${args[1]}': ${errnoMsg.EBUSY}`, ""]);
        return errno.EBUSY;
      }
      let wdTreeSource = findDir(args[0], 1);
      if (wdTreeSource == null || !wdTreeSource.hasChild(fileSource)) {
        stdout.append([`mv: can't rename '${args[0]}': ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }
      let toMv = wdTreeSource.children[fileSource];
      // Destination
      let fileDest = getName(args[1]);
      let wdTreeDest = findDir(args[1], 1);
      if (wdTreeDest == null) {
        stdout.append([`mv: can't rename '${args[0]}': ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }
      if (wdTreeDest.hasChild(fileDest) && wdTreeDest.children[fileDest].type == "dir") {
        wdTreeDest = wdTreeDest.children[fileDest];
        fileDest = fileSource;
      }

      let toMove = wdTreeSource.path != wdTreeDest.path;
      let toRename = fileSource != fileDest;
      // Move first, eventually
      if (toMove) {
        if (!toMv.move(wdTreeDest, toRename)) {
          stdout.append([`mv: cannot move a directory to a subdirectory of itself`, ""]);
          return errno.EINVAL;
        }
      }
      // Rename, eventually
      if (toRename) toMv.rename(fileDest);
      // Réparer par la suite, si un fichier a voulu être bougé et renommé mais
      // Qu'un fichier existait au nouvel endroit avec l'ancien nom
      if (toMove && toRename) {
        let toFix = wdTreeDest.children[fileSource + "-TMPMV$!$!"];
        if (toFix) toFix.rename(fileSource);
      }

      return 0;
    }
  },

  "chmod": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args, ["-r", "-w", "-x"]);

      if (args.length < 2) {
        stdout.append([`chmod: missing operand`, ""]);
        return errno.E2SMALL;
      }
      let wdTree = find(args[1]);
      if (wdTree == null) {
        stdout.append([`chmod: cannot access ${args[1]}: ${errnoMsg.ENOENT}`, ""]);
        return errno.ENOENT;
      }

      // Cas, par exemple, de chmod a+wx foo
      // Ne prend pas en charge, par exemple, g+w-r
      let parts = args[0].split("+");
      if (parts.length != 2) parts = args[0].split("-");
      if (parts.length == 2) {
        let whoNeedsChange = [];
        for (let char of parts[0]) {
          if (char == "u") whoNeedsChange.push(0);
          else if (char == "g") whoNeedsChange.push(1);
          else if (char == "o") whoNeedsChange.push(2);
          else if (char == "a") whoNeedsChange.push(0, 1, 2);
          else {
            stdout.append([`chmod: invalid mode: '${args[0]}'`, ""]);
            return errno.EINVAL;
          }
        }
        if (parts[0].length === 0) whoNeedsChange.push(0, 1, 2);

        let bitsToChange = [];
        for (let char of parts[1]) {
          if (char == "r") bitsToChange.push(4);
          else if (char == "w") bitsToChange.push(2);
          else if (char == "x") bitsToChange.push(1);
          else {
            stdout.append([`chmod: invalid mode: '${args[0]}'`, ""]);
            return errno.EINVAL;
          }
        }

        for (let who of whoNeedsChange) {
          for (let bit of bitsToChange) {
            if (/\+/.test(args[0])) {
              if (!(wdTree.permissions[who] & bit)) wdTree.permissions[who] += bit;
            } else {
              if (wdTree.permissions[who] & bit) wdTree.permissions[who] -= bit;
            }
          }
        }

        return 0;
      }

      // Cas, par exemple, de chmod 777 bar
      else if (args[0].length == 3 && !isNaN(args[0])) {
        wdTree.permissions = [Number(args[0][0]), Number(args[0][1]), Number(args[0][2])];
        return 0;
      }

      // Sinon, plus d'espoir d'avoir eu une entrée correcte
      else {
        stdout.append([`chmod: invalid mode: '${args[0]}'`, ""]);
        return errno.EINVAL;
      }
    }
  },

  "chroot": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append([`chroot: missing operand`, ""]);
        return errno.E2SMALL;
      }

      let wdTree = find(args[0]);
      if (wdTree.type == "file") {
        stdout.append([`chroot: ${args[0]}: ${errnoMsg.EISFILE}`, ""]);
        return errno.EISFILE;
      }

      wdTree.name = "";
      wdTree.user = "root";
      wdTree.group = "root";
      wdTree.isRoot = true;
      wdTree.permissions = [7, 5, 5];
      wdTree.parent = wdTree;
      wdTree.children[".."] = wdTree.parent;
      wdTree.updatePath("/");
      rootDir = wdTree;
      cwd = "/";
      envVar["PWD"] = cwd;

      return 0;
    }
  },

  "alias": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      let outputArr = [];
      if (args.length == 0) {
        for (let alias of Object.keys(aliases)) {
          outputArr.push(`alias ${alias}='${aliases[alias]}'`);
        }
      } else {
        for (let arg of args) {
          if (arg.indexOf("=") != -1) {
            let parts = arg.split("=");
            aliases[parts[0]] = parts[1];
          } else if (aliases.hasOwnProperty(arg)) {
            outputArr.push(`alias ${arg}='${aliases[arg]}'`);
          } else {
            outputArr.push(`-bash: alias: ${arg}: not found`);
          }
        }
      }

      outputArr.push("");
      stdout.append(outputArr);

      return 0;
    }
  },

  "pm-term": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      let options = Parser.getOptions(args).list;
      args = Parser.getNonOptions(args);

      let invalidMsg = [`pm-term: invalid usage`, `Try 'pm-term --help' for more information.`, ""];
      let seekingHelp = options.includes("--help");
      if (args.length == 0 && !seekingHelp) {
        stdout.append(invalidMsg);
        return errno.EINVAL;
      }

      if (seekingHelp) {
        stdout.append([`Usage: pm-term [options] {bgcolor|textcolor} hexColor`,
          `Permet de configurer le terminal.`,
          ""
        ]);
        return 0;
      } else if (args[0] == "textcolor") {
        let color = fixHexColor(args[1]);
        if (color) {
          txtColor = color;
          fill(txtColor);
          term.dispAll();
        } else {
          stdout.append(invalidMsg);
          return errno.EINVAL;
        }
      } else if (args[0] == "bgcolor") {
        let color = fixHexColor(args[1]);
        if (color) {
          bgColor = color;
          term.dispAll();
        } else {
          stdout.append(invalidMsg);
          return errno.EINVAL;
        }
      } else {
        stdout.append(invalidMsg);
        return errno.EINVAL;
      }
    }
  },

  "reset-fs": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      if (User.current != "root") {
        stdout.append(["Warning, running this command will reset the file system.", "Please run it with root privileges.", ""]);
        return errno.EPERM;
      } else {
        initFs();
        cwd = envVar["HOME"];
        envVar["PWD"] = cwd;
        stdout.append(["The file system has been reset", ""]);
        return 0;
      }
    }
  },

  "browser": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      args = Parser.getNonOptions(args);

      if (args.length == 0) {
        stdout.append(["browser: missing argument", ""]);
        return errno.E2SMALL;
      }

      let url = args[0];
      let file = find(url);

      if (file != null) {
        let newWindow = window.open("");
        if (file.type == "dir") {
          if (file.childCount == 0) newWindow.document.write(`<h2>${file.path}: empty directory</h2>`);
          else if ("index.html" in file.children && file.children["index.html"].type == "file")
            newWindow.document.write(file.children["index.html"].buffer);
          else
            newWindow.document.write(`<h2>${file.path}: directory</h2><br><b>Children:</b><br>${Object.keys(file.children).join('<br>')}`);
        } else {
          newWindow.document.write(file.buffer);
        }
      } else window.open(url);

      return 0;
    }
  },

  "export": {
    id: maxCommandId++,
    exec: function (stdout, args) {
      for (let arg of args) {
        const parts = arg.split("=");
        if (parts.length !== 2) continue;
        envVar[parts[0]] = parts[1];
      }

      return 0;
    }
  }
};

// Takes bash text (could be an entire file buffer) and executes everything
// Optional argument: displayPromptAfter (explicit), stdout : the only output stream
function exec(text, displayPromptAfter = true, stdout = term.stdin) {
  let parsedCommands = Parser.parse(text);

  for (let parsedCommand of parsedCommands)
    exitCode = execParsed(parsedCommand, stdout);

  if (displayPromptAfter) promptAt(pointer.at[0], true);
}

// Takes a finished ParsedCommand and an output stream, executes it, and
// returns the command exit code
function execParsed(parsedCommand, stdout = term.stdin) {
  if (parsedCommand.state !== ParsedCommand.STATUS_PARSED)
    return errno.EINVAL;

  if (parsedCommand.requiredExitCode !== null && exitCode != parsedCommand.requiredExitCode)
    return exitCode;

  if (!commands.hasOwnProperty(parsedCommand.key)) {
    let wd = find(parsedCommand.key);
    if (wd !== null && parsedCommand.key.includes('/')) {
      if (wd.type == "dir") {
        stdout.append([`${parsedCommand.key}: ${errnoMsg.EISDIR}`, ""]);
        return errno.EISDIR;
      } else if (wd.type == "file") {
        if (!wd.hasPermission("x")) {
          stdout.append([`${parsedCommand.key}: ${errnoMsg.EACCES}`, ""]);
          return errno.EACCES;
        }
        return execFile(wd, stdout);
      }
    }
    else {
      stdout.append([`${parsedCommand.key}: command not found`, ""]);
      return errno.EUNKNW;
    }
  }

  // Command execution
  if (parsedCommand.redirectMode === Parser.NO_REDIRECT) {
    return commands[parsedCommand.key].exec(stdout, parsedCommand.arguments) || 0;
  } else {
    let virtualStream = new VirtualStream();
    let fileStream = new FileStream(parsedCommand.redirectTarget);

    // BUG: if we get an ENOENT from the FileStream later, the command was still executed.
    // We should prevent the command execution is a test-write to the FileStream fails
    let commandExitCode = commands[parsedCommand.key].exec(virtualStream, parsedCommand.arguments);
    let writeExitCode;

    if (parsedCommand.redirectMode === Parser.REDIRECT_SIMPLE) {
      writeExitCode = fileStream.write(virtualStream.read(virtualStream.size));
    } else {
      writeExitCode = fileStream.append(virtualStream.read(virtualStream.size));
    }

    if (writeExitCode === errno.ENOENT) {
      stdout.append([`bash: ${parsedCommand.redirectTarget}: ${errnoMsg.ENOENT}`, ""]);
      return writeExitCode;
    } else if (writeExitCode === errno.EISDIR) {
      stdout.append([`bash: ${parsedCommand.redirectTarget}: ${errnoMsg.EISDIR}`, ""]);
      return writeExitCode;
    } else if (writeExitCode === 0) {
      return commandExitCode || 0;
    }
  }

  // If we get here anyways, nothing happened 🤷
  return 0;
}

function execFile(file, stdout = term.stdin) {
  if (file.buffer.length == 0) return 0;

  // Testing for shebang
  const firstLine = file.buffer.split("\n")[0];
  if (firstLine.substr(0, 2) == "#!") {
    const execWith = firstLine.replace("#!", "").trim();
    // Fake hardcoded runtime to execute JS code
    if (execWith == "/usr/bin/js") {
      stdout.append([eval(file.buffer.replace(/.*\n/, "")), ""]);
      return 0;
    // For now, in every other case, we'll execute bash code
    } else {
      // TODO
    }
  }
  
  stdout.append(["File execution not yet implemented", ""]);
  return 1;
}
