class Pointer {
  insert = false;
  pos = createVector();
  text = "_";
  blink = setInterval(() => {
    if (this.text != " ") this.text = " ";
    else this.text = (this.insert ? "|" : "_");
    this.render();
  }, 500);
  screen;
  at;

  constructor(screen, x = 0, y = 0) {
    if (!screen) throw "Please attach a screen to the pointer";
    this.screen = screen;
    this.at = [y, x];
  }

  render() {
    if (this.screen.focused) {
      if (this.screen.inGrid(this.at[1], this.at[0]))
        this.screen.cells[this.at[0]][this.at[1]].render();
      let xOffset = this.insert ? -5 : Number(cellSize[0] > 11);
      text(this.text, this.pos.x + xOffset, this.pos.y + cellSize[1] - 5);
    }
  }

  updatePos() {
    // Pour effacer l'ancien curseur si il était dessiné
    this.oldAt = [floor(this.pos.y / cellSize[1]), floor(this.pos.x / cellSize[0])];
    if (this.screen.inGrid(this.oldAt[1], this.oldAt[0]))
      this.screen.cells[this.oldAt[0]][this.oldAt[1]].render();

    this.text = this.insert ? "|" : "_";
    this.pos.x = this.at[1] * cellSize[0];
    this.pos.y = this.at[0] * cellSize[1];
    this.render();
  }
}