class User {
  username;
  password;
  uid;
  gid;
  info;
  homeDirectory;
  shell;

  constructor(username, password, uid, gid, info, homeDirectory, shell) {
    if (!username) throw "New user must have a name";
    if (uid == -1) uid = User.lastUID++;
    if (gid == -1) gid = uid;
    if (!homeDirectory || homeDirectory == "") homeDirectory = `/home/${username}`;
    if (!shell || shell == "") shell = "/usr/bin/bash";

    this.username = username;
    this.password = password;
    this.uid = uid;
    this.gid = gid;
    this.info = info;
    this.homeDirectory = homeDirectory;
    this.shell = shell;
  }

  static get current() {
    return User.sessions[User.sessions.length - 1];
  }
}

User.lastUID = 1000;
User.sessions = [];